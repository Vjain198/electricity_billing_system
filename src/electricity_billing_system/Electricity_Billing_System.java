/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electricity_billing_system;

/**
 *
 * @author vikas jain
 */
import java.util.*;
public class Electricity_Billing_System {
    public static void main(String[] args) {
        try{
          
            double  units,amount = 0;
            String  customer_number , customer_name , connection_type;
            int previous_reading , current_reading;
            
            Scanner input = new Scanner(System.in);
            
            System.out.println("Enter Customer no.");
            customer_number = input.next();
            
            System.out.println("Enter Customer name.");
            customer_name = input.next();
            
            System.out.println("Enter previous month reading");
            previous_reading = input.nextInt();
            
            System.out.println("Enter current month reading");
            current_reading = input.nextInt();
            
            System.out.println("Enter Customer type (Domestic/Commercial)");
            connection_type = input.next();
            
            
            units = current_reading - previous_reading;
            
            if(connection_type.equals("Domestic"))
            {
                if(units<=0)
                    amount = 0;
                else if(units<=100)
                    amount = units*2;
                else if(units <=200)
                    amount = units*4.5;
                else if(units<=500)
                    amount = units*6;
                else
                    amount = units*7;
                            
            }else if(connection_type.equals("Commercial")){
                if(units<=0)
                    amount = 0;
                else if(units<=100)
                    amount = units*2;
                else if(units <=200)
                    amount = units*2.5;
                else if(units<=500)
                    amount = units*4;
                else
                    amount = units*6;
                
            }
            System.out.println("===============================================================================");
            
            System.out.println("           Dear Customer Your  This Month Electricity Bill Details.            ");
            
            System.out.println("===============================================================================");
            
            System.out.println("     Customer Number is : " + customer_number);
            
            System.out.println("     Customer Name is :  " +  customer_name );
            
            System.out.println("     Customer Previous Month Reading is :   " + previous_reading);
            
            System.out.println("     Customer Current Month Reading is :   " + current_reading);
            
            System.out.println("     Customer Total Units is :      " + units);
            
            System.out.println("     Your Thi Month Bill is:        " + amount);
            
            System.out.println("====================================================================================================");

     
        }
    catch(Exception e){
        System.out.println(e);
    }
    }
}
